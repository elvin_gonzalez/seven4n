package com.seven4n.robobum.exception;

/**
 *
 * @author Elvin
 */
public class ArgumentException extends Exception{

    public ArgumentException() {
    }

    public ArgumentException(String message) {
        super(message);
    }
}