package com.seven4n.robobum.exception;

/**
 *
 * @author Elvin
 */
public class ThreatPositionException extends RuntimeException{

    public ThreatPositionException() {
    }

    public ThreatPositionException(String message) {
        super(message);
    }
    
}