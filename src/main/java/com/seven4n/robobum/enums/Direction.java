package com.seven4n.robobum.enums;

/**
 *
 * @author Elvin
 */
public enum Direction {
    NORTH("N"),
    EAST("E"),
    WEST("O"),
    SOUTH("S"),
    DEFAULT("");
    
    private final String value;

    private Direction(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public static Direction getInstance(String value){
        Direction result = Direction.DEFAULT;
        for(Direction direction : values()){
            if(direction.value.equalsIgnoreCase(value)){
                result = direction;   
            }
        }
        
        return result;
    }
}
