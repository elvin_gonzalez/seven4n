package com.seven4n.robobum.enums;

/**
 *
 * @author Elvin
 */
public enum Movement {
    RIGHT ("D"),
    LEFT("I"),
    ADVANCE("A"),
    DEFAULT("");
    
    private final String value;
    
    private Movement(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static Movement getInstance(String value){
        Movement result = Movement.DEFAULT;
        for(Movement movement : values()){
            if(movement.value.equalsIgnoreCase(value)){
                result = movement;   
            }
        }
        
        return result;
    }
}
