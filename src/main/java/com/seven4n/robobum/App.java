package com.seven4n.robobum;

import com.seven4n.robobum.dto.Position;
import com.seven4n.robobum.dto.Threat;
import com.seven4n.robobum.enums.Direction;
import com.seven4n.robobum.enums.Movement;
import com.seven4n.robobum.exception.ArgumentException;
import com.seven4n.robobum.exception.ThreatPositionException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App 
{
    //Used to read the standard input
    private final static Scanner inputScanner = new Scanner(System.in);
    
    //Pattern to match the format of the threats in the file
    private final static Pattern threatPattern = Pattern.compile("^\\((?<x>\\d),(?<y>\\d)\\)(?<value>.)$");
    
    //Path to the threats file. Defaults to ./amenazas.txt
    private static Path threatsFilepath = Paths.get(System.getProperty("user.dir"), "amenazas.txt");
    
    public static void main( String[] args )
    {
        try {
            readApplicationArgs(args);
            
            List<Threat> threats = readThreatsFile();
            
            System.out.println("Introduzca coordenada superior derecha del terreno:");
            final char[][]grid = loadGrid(readGridSize(), threats);
            
            Robot robot = new Robot(grid);
            
            do {
                System.out.println("Introduzca la posicion inicial del robot:");
                robot.setInitialPosition(readInitialPosition());

                System.out.println("Introduzca los movimientos del robot:");
                Queue<Movement> movements = readMovements();
                
                //Using a hashset to prevent adding multiple times the same Threat
                Set<Threat> foundThreats = new HashSet<>();
                
                Movement move;
                while((move = movements.poll()) != null){
                    if(robot.detect()){
                        foundThreats.add(robot.getThreat());
                    }
                    robot.move(move);
                }
                
                Position finalPosition = robot.getPosition();
                System.out.println("Posicion final:");
                System.out.println(String.format("%d %d %s", finalPosition.x, finalPosition.y, finalPosition.direction.getValue()));
                System.out.println("Amenazas detectadas:");
                for(Threat t : foundThreats){
                    System.out.printf("(%d,%d)", t.position.x, t.position.y);
                }
                System.out.println();
                System.out.println("¿Desea ejecutar de nuevo? (Y/N)");

            } while(inputScanner.nextLine().equalsIgnoreCase("Y"));
        } catch (IOException ex) {
            System.err.println("No se encontro archivo de amenzanas");
        } catch (ArgumentException|ThreatPositionException ex) {
            System.err.println(ex.getMessage());
        }finally{
            inputScanner.close();
        }
    }
    /**
     * Read the arguments passed to the application at runtime
     * @param args 
     */
    private static void readApplicationArgs(String[] args){
        //Set the path to the threats file, if present in the application args
        if(args.length > 0){
            threatsFilepath = Paths.get(args[0]);
        }
    }
    
    /**
     * Read the threats file and return them as a List of Treats. 
     * If no threats are found (or are not correctly formated) returns an empty list.
     * This method can only throw IOExcetion while reading the file.
     * @return The list of threats read from the file.
     * @throws IOException If the field cannot be found
     */
    private static List<Threat> readThreatsFile() throws IOException{
        List<Threat> threats = new ArrayList<>();
        List<String> lines = Files.readAllLines(threatsFilepath);
        for(String line : lines){
            Matcher matcher = threatPattern.matcher(line);
            if(matcher.matches()){
                Position p = new Position();
                p.x = Integer.parseInt(matcher.group("x"));
                p.y = Integer.parseInt(matcher.group("y"));
                p.direction = Direction.DEFAULT;
                
                Threat t = new Threat();
                t.position = p;
                t.value = matcher.group("value").charAt(0);
                
                threats.add(t);
            }
        }
        return threats;
    }
    
    /**
     * Read the grid size from the standard input and construct a new grid 
     * represented by a two dimensional char array
     * @return The char array representation of the grid.
     * @throws ArgumentException if the inputs are not two integer values separated by a space
     */
    private static char[][] readGridSize() throws ArgumentException{
        try{
            String[] coordinates = inputScanner.nextLine().split(" ");
            
            int maxX = Integer.parseInt(coordinates[0]) + 1;
            int maxY = Integer.parseInt(coordinates[1]) + 1;
            return new char[maxX][maxY];
        }catch(NumberFormatException|IndexOutOfBoundsException ex){
            throw new ArgumentException("Introduzca una tamaño valido para el terreno, ej: 2 2");
        }
    }
    
    /**
     * Set the threats into the grid as the characters
     * @param grid The grid
     * @param threats The list of threats
     * @return The grid loaded with the threats
     * @throws ThreatPositionException if the positions of the Threats are outside the size of the grid
     */
    private static char[][] loadGrid(char[][] grid, List<Threat> threats) throws ThreatPositionException{
        try{
            for(Threat t : threats){
                grid[t.position.x][t.position.y] = t.value;
            }
            return grid;
        }catch(ArrayIndexOutOfBoundsException e){
            throw new ThreatPositionException("La posicion de las amenazas sobrepasan el tamaño del terreno");
        }
    }
    
    /**
     * Reads the initial position for the robot from the standard input 
     * @return The initial Position
     * @throws ArgumentException If the position is not correctly formated
     */
    private static Position readInitialPosition() throws ArgumentException{
        try{
            String[] inputs = inputScanner.nextLine().split(" ");

            Position initialPosition = new Position();
            initialPosition.x = Integer.parseInt(inputs[0]);
            initialPosition.y = Integer.parseInt(inputs[1]);
            
            Direction direction = Direction.getInstance(inputs[2]);
            if(direction.equals(Direction.DEFAULT)){
                throw new ArgumentException("Introduzca una posicion inicial valida. Ej: 1 2 N");
            }
            initialPosition.direction = direction;
            return initialPosition;
        }catch(NumberFormatException|IndexOutOfBoundsException e){
            throw new ArgumentException("Introduzca una posicion inicial valida. Ej: 1 2 N");
        }
    }
    
    /**
     * Reads the list of movements from the standard input
     * @return
     * @throws ArgumentException 
     */
    private static Queue<Movement> readMovements() throws ArgumentException{
        Queue<Movement> movements = new LinkedList<>();
        String[] commands = inputScanner.nextLine().split("");
        for(String command : commands){
            Movement movement = Movement.getInstance(command);
            if(movement.equals(Movement.DEFAULT)){
                throw new ArgumentException("Introduzca una serie de movimientos validos. Ej: IAIADDA");
            }
            movements.add(movement);
        }
        return movements;
    }
}
