package com.seven4n.robobum;

import com.seven4n.robobum.dto.Position;
import com.seven4n.robobum.dto.Threat;
import com.seven4n.robobum.enums.Direction;
import com.seven4n.robobum.enums.Movement;
import com.seven4n.robobum.exception.ArgumentException;
import java.util.Arrays;
import util.CircularList;

/**
 *
 * @author Elvin
 */
public class Robot {
    //Uses a circular list so we can rotate in any direction when moving
    private final static CircularList<Direction> directions = 
            new CircularList<>(Arrays.asList(
                    Direction.NORTH, 
                    Direction.EAST, 
                    Direction.SOUTH, 
                    Direction.WEST));
    
    private final char[][] grid;
    private Position position;

    public Robot(char[][] grid) {
        this.grid = grid;
    }

    public Position getPosition() {
        return position;
    }

    /**
     * Sets the robot's initial position in the grid
     * @param position The initial position
     * @throws ArgumentException if the initial position is outside the grid
     */
    public void setInitialPosition(Position position) throws ArgumentException {
        if(position.x > grid.length || position.y > grid[0].length){
            throw new ArgumentException("La posicion inicial del robot esta fuera del terreno");
        }
        this.position = position;
    }
    
    /**
     * Makes the robot move through the grid. The accepted movements include 
     * turning to the left or right, and moving forward, any other movement 
     * is ignored.
     * @param movement the movement command 
     */
    public void move(Movement movement){        
        switch(movement){
            case RIGHT:
                position.direction = directions.next(position.direction);
                break;
            case LEFT:
                position.direction = directions.previous(position.direction);
                break;
            case ADVANCE:
                advance();
                break;
        }
    }
    
    /**
     * Makes the robot move forward in its actual direction. 
     * If the movement would make the robot step outside the grid, the 
     * movement is ignored and the robot stays in place.
     */
    private void advance(){
        switch(position.direction){
            case EAST:
                position.x = (position.x < this.grid.length) ? position.x + 1 : position.x;
                break;
            case WEST:
                position.x = (position.x > 0) ? position.x - 1 : position.x;
                break;
            case NORTH:
                position.y = (position.y < this.grid[position.x].length) ? position.y + 1 : position.y;
                break;
            case SOUTH:
                position.y = (position.y > 0) ? position.y - 1 : position.y;
                break;
        }
    }
    
    /**
     * The robot detects any threat in its current position.
     * @return true if there is a threat in its current position of the grid, 
     * false otherwise
     */
    public boolean detect(){
        return grid[position.x][position.y] == '*';
    }
    
    /**
     * Returns any Threat found in the robot's current position
     * @return The detected threat
     */
    public Threat getThreat(){
        Threat threat = new Threat();
        Position pos = new Position();
        pos.x = this.position.x;
        pos.y = this.position.y;
        threat.position = pos;
        threat.value = grid[pos.x][pos.y];
        return threat;
    }
}
