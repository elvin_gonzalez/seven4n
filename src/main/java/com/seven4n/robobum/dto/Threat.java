package com.seven4n.robobum.dto;

import java.util.Objects;

/**
 *
 * @author Elvin
 */
public class Threat {
    public Position position;
    public char value;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.position);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Threat other = (Threat) obj;
        return Objects.equals(this.position, other.position);
    }
}
