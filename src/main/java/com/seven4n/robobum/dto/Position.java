package com.seven4n.robobum.dto;

import com.seven4n.robobum.enums.Direction;

/**
 *
 * @author Elvin
 */
public class Position {
    public int x;
    public int y;
    public Direction direction;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.x;
        hash = 37 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Position other = (Position) obj;
        if (this.x != other.x) {
            return false;
        }
        return this.y == other.y;
    }
}
