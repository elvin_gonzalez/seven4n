package util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A quick-and-dirty implementation of a circular list using an array as base,
 * so it can be traversed forward or backward indefinitely.
 * @author Elvin
 * @param <T> The type of the content
 */
public class CircularList<T> extends ArrayList<T>{

    public CircularList(int initialCapacity) {
        super(initialCapacity);
    }

    public CircularList() {
    }

    public CircularList(Collection<? extends T> c) {
        super(c);
    }

    @Override
    public T get(int index) {
        return super.get(index >= 0  ? index % size() : size() + index);
    }
    
    /**
     * Return value next to the given one
     * @param value
     * @return 
     */
    public T next(T value){
        return get(indexOf(value) + 1);
    }
    
    /**
     * Return value previous to the given one
     * @param value
     * @return 
     */
    public T previous(T value){
        return get(indexOf(value) - 1);
    }
}