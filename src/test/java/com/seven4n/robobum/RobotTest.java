package com.seven4n.robobum;

import com.seven4n.robobum.dto.Position;
import com.seven4n.robobum.enums.Direction;
import com.seven4n.robobum.enums.Movement;
import com.seven4n.robobum.exception.ArgumentException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class RobotTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public RobotTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( RobotTest.class );
    }

    /**
     * The robot must not detect any threat in an empty position
     * @throws ArgumentException 
     */
    public void testDetect() throws ArgumentException
    {
        Robot robot = new Robot(new char[1][1]);
        Position p = new Position();
        p.x = 0;
        p.y = 0;
        p.direction = Direction.NORTH;
        robot.setInitialPosition(p);
        assertFalse( robot.detect() );
    }
    
    /**
     * The robot must be able to detect a threat in position with a bomb (character '*')
     * @throws ArgumentException 
     */
    public void testDetectThreat() throws ArgumentException
    {
        Robot robot = new Robot(new char[][] {{'*'}});
        Position p = new Position();
        p.x = 0;
        p.y = 0;
        p.direction = Direction.NORTH;
        robot.setInitialPosition(p);
        
        assertTrue( robot.detect() );
    }
    
    /**
     * Given a turn left command, the robot must turn to the expected direction 
     * while staying in the same position
     * @throws ArgumentException 
     */
    public void testTurnLeft() throws ArgumentException{
        Position initialPosition = new Position();
        initialPosition.x = 0;
        initialPosition.y = 0;
        initialPosition.direction = Direction.NORTH;
        
        //Turning left from NORTH should result in facing to WEST
        Position expetecPosition = new Position();
        expetecPosition.x = 0;
        expetecPosition.y = 0;
        expetecPosition.direction = Direction.WEST;
        
        Robot robot = new Robot(new char[][] {{'*'}});
        robot.setInitialPosition(initialPosition);
        robot.move(Movement.LEFT);
        
        assertEquals(expetecPosition, robot.getPosition());
        assertEquals(expetecPosition.direction, robot.getPosition().direction);
    }
    
    /**
     * Given an advance command, the robot must move forward to the direction it
     * is facing.
     * @throws ArgumentException 
     */
    public void testAdvance() throws ArgumentException{        
        Position initialPosition = new Position();
        initialPosition.x = 0;
        initialPosition.y = 0;
        initialPosition.direction = Direction.NORTH;
        
        //Advancing to the NORTH should result in moving the robot 1 space 
        //in the Y axis while keeping the same X axis and direction
        Position expectedPosition = new Position();
        expectedPosition.x = 0;
        expectedPosition.y = 1;
        expectedPosition.direction = Direction.NORTH;
        
        Robot robot = new Robot(new char[2][2]);
        robot.setInitialPosition(initialPosition);
        robot.move(Movement.ADVANCE);

        assertEquals(robot.getPosition(), expectedPosition);
        assertEquals(robot.getPosition().direction, expectedPosition.direction);
    }
}
